#coding: utf-8
'''
當 CherryPy 程式尚未牽涉 'OPENSHIFT_DATA_DIR' 資料存取時, 近端與遠端程式完全相同.
但若牽涉必須透過網際介面進行 persistent 資料存取時, 則必須區分近端 data 目錄與遠端目錄.
本程式執行需要 brython.py 與 menu.py
'''
import cherrypy
import os
# 將同目錄下的 brython.py 導入
import sys
# 確定程式檔案所在目錄
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
# 將目前檔案所在目錄納入 sys 模組搜尋目錄中
sys.path.append(_curdir)
import brython
# 將同目錄下的 menu.py 導入
import menu

# 以下為產生 html 標註的特定函式
def htmlTitle(title):
    return '''
<!DOCTYPE html>
<html>
<head>
<! charset meta 設定應該在 title 標註之前, 以免因產生編碼錯誤的 title -->
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>'''+title+"</title>"

def htmlCSS(css):
    return '''
<link rel="stylesheet" type="text/css" href="'''+css+'''">
</head>
'''

class Brython(object):

    @cherrypy.expose
    def WebLink(self):
        return htmlTitle("WebLink")+'''
        </head>
        <body>

        <h2>Pro/Web.Link:</h2>
        <h3>●檔案下載:</h3>
        <a href="downloads/cadp_weblink_python_v1.7z">weblink</a></br>
        <h3>●操作過程:</h3>
        <h4>1.編寫程式 → go</h4>
        <img src="/static/images/WebLink_1.jpg"  height="350" width="500"></img>
        <br />
        <h4>2.網站設為信任網站</h4>
        <img src="/static/images/WebLink_2.jpg"   height="350" width="500"></img>
        <br />
        <h4>3.定義尺寸變數名稱 → 存檔 </h4>
        <img src="/static/images/WebLink_3.jpg"   height="350" width="500"></img>
        <br />
        <h4>4.在creo上開啟網頁 → 即可執行零件體積 </h4>
        <img src="/static/images/WebLink_4.jpg"  t height="350" width="500"></img>
        <br />
        <h4>5.程式已自動存檔4個變數的零件體積 </h4>
        <img src="/static/images/WebLink_5.jpg"  t height="350" width="500"></img>
        <br />
        </body>
        </html>
        '''

    @cherrypy.expose
    def creo_part(self):
        return htmlTitle("creo_part")+'''
        </head>
        <body>
        <!--<a href="http://google.com">go google</a>-->

        <h2>creo-ex14零件:</h2>
        
        <h3>●檔案下載:</h3>
        <a href="downloads/creo_ex14.prt.1">creo_ex14</a></br>
        
        <h3>●工程圖:</h3>
        <img src="/static/images/creo_ex14.jpg"  height="350" width="500"></img>
        <br />

        <h3>●零件繪圖過程:</h3>
        <h4>1.畫前面的草圖 → 長料</h4>
        <img src="/static/images/creo_ex14_1.jpg"  height="350" width="500"></img>
        <br />
        <h4>2.畫兩小圓 → 除料</h4>
        <img src="/static/images/creo_ex14_2.jpg"   height="350" width="500"></img>
        <br />
        <h4>3.畫兩小三角型，小圓 → 除料</h4>
        <img src="/static/images/creo_ex14_3.jpg"   height="350" width="500"></img>
        <br />
        <h4>4.完成</h4>
        <img src="/static/images/creo_ex14_4.jpg"  t height="350" width="500"></img>
        <br />
        
        <h3>●影片:</h3>
        <iframe width="1280" height="720" src="//www.youtube.com/embed/CTQVXN5uP-w" frameborder="0" allowfullscreen></iframe><br />
        </body>
        </html>
        '''
    @cherrypy.expose
    def creo_Assembly(self):
        return htmlTitle("creo_Assembly")+'''
        </head>
        <body>
        <h2>creo組立:</h2>
        <h3>●檔案下載:</h3>
        <a href="downloads/creo_Assembly.asm.4">creo組立圖</a></br>
        <h3>●組立圖:</h3>
        <img src="/static/images/creo_Assembly.jpg"   height="350" width="500"></img><br />
        <h3>● 組立過程</h3> 
        <h3>1.匯入零件</h3> 
        <img src="/static/creo_Assembly_1.jpg"   height="350" width="500"></img></br>
        <h3>2匯入零件1.5.7與底板組裝約束接合</h3>
        <img src="/static/creo_Assembly_2.jpg"   height="350" width="500"></img></br>
        <h3>3.匯入零件2.8 → 約束接合</h3> 
        <img src="/static/images/creo_Assembly_3.jpg"   height="350" width="500"></img></br>
        <h3>4.完成</h3>
        <img src="/static/images/creo_Assembly_4.jpg"   height="350" width="500"></img></br>
        </body>
        </html>
        '''
    @cherrypy.expose
    def solvespace_part(self):
        return htmlTitle("solvespace_part")+'''
        </head>
        <body>
        <h2>solvespace-ex13零件:</h2>
        
        <h3>●檔案下載:</h3>
        <a href="downloads/solvespace-ex13.slvs">solvespace-ex13</a></br>
        
        <h3>●工程圖:</h3>
        <img src="/static/images/solvespace-ex13.jpg"   height="350" width="500"></img>
        <br />

        <h3>●零件繪圖過程:</h3>
        <h4>1.畫鋸齒草圖 → 環形陣列 → 長料</h4>
        <img src="/static/images/solvespace-ex13_1.jpg"   height="350" width="500"></img>
        <br />
        <h4>2.畫大圓 → 長料，畫小圓 → 除料</h4>
        <img src="/static/images/solvespace-ex13_2.jpg"   height="350" width="500"></img>
        <br />
        <h4>3.畫鍵槽 → 除料</h4>
        <img src="/static/images/solvespace-ex13_3.jpg"   height="350" width="500"></img>
        <br />
        <h4>4.完成</h4>
        <img src="/static/images/solvespace-ex13_4.jpg"   height="350" width="500"></img>
        <br />
        
        <h3>●影片:</h3>
        <iframe width="500" height="350" src="//www.youtube.com/embed/imyMOK3XDMk" frameborder="0" allowfullscreen></iframe><br />
        </body>
        </html>
        '''
    @cherrypy.expose
    def solvespace_Assembly(self):
        return htmlTitle("solvespace_Assembly")+'''
        </head>
        <body>
        <h2>solvespace組立:</h2>
        <h3>●檔案下載:</h3>
        <a href="downloads/solvespace-Assembly.slvs">solvespace組合圖</a></br>
        <h3>● 組立過程</h3> 
        <h3>1.匯入底座零件與隔板零件 → 約束接合</h3> 
        <img src="/static/images/solvespace_Assembly_1.jpg"   height="350" width="500"></img></br>
        <h3>2.陣列隔板</h3>
        <img src="/static/images/solvespace_Assembly_2.jpg"   height="350" width="500"></img></br>
        <h3>3.匯入背板零件 → 約束接合</h3> 
        <img src="/static/images/solvespace_Assembly_3.jpg"   height="350" width="500"></img></br>
        <h3>4.完成</h3>
        <img src="/static/images/solvespace_Assembly_4.jpg"   height="350" width="500"></img></br>
        </body>
        </html>
        '''
    @cherrypy.expose
    def Brython(self):
        return htmlTitle("Brython")+'''
        </head>
        <body>
        <h2>Brython:</h2>
        <h3>●ex1:</h3>
        <img src="/static/images/brython_1.jpg"   height="350" width="500"></img><br />
        <h3>●ex2:</h3>
        <img src="/static/images/brython_2.jpg"   height="350" width="500"></img><br />
        </body>
        </html>
        '''
    @cherrypy.expose
    def V_REP(self):
        return htmlTitle("V-REP")+'''
        </head>
        <body>
        <h2>V-REP:</h2>
        <h3>●檔案下載:</h3>
        <a href="downloads/bubbleRob.ttt">V-REP_bubbleRob</a></br>
        <h3>●操作過程:</</h3>
        <h3>1.創建球體，設定BubbleRob </h3> 
        <img src="/static/images/v-rep1.jpg"   height="350" width="500"></img><br />
        <h3>2.建立障礙物</h3> 
        <img src="/static/images/v-rep2.jpg"   height="350" width="500"></img><br />
        <h3>3.佈置障礙物</h3> 
        <img src="/static/images/v-rep3.jpg"   height="350" width="500"></img><br />
        <h3>4.測驗，完成</h3> 
        <img src="/static/images/v-rep4.jpg"   height="350" width="500"></img><br />
        </body>
        </html>
        '''
    @cherrypy.expose
    def about(self):
        return htmlTitle("有關本網站")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
本網站為電腦輔助設計實習課程分組網站<br />
採用 CherryPy 與 Python 建置<br />
</body>
</html>
'''

    @cherrypy.expose
    def index(self):
        return htmlTitle("Brython and Pulldown menu")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
'''+brython.BrythonConsole()+ \
'''
</body>
</html>
'''

    @cherrypy.expose
    def creoParts(self):
        return htmlTitle("Creo 零件檔案")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
這裡展示 Creo 零件檔案
</body>
</html>
'''

    @cherrypy.expose
    def introMember1(self):
        return htmlTitle("組員 1 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 王若庭<br />
組員學號: 40123101<br />
組員相片:<br /><img src="/static/images/member1.jpg"></img><br />


</body>
</html>
'''

    @cherrypy.expose
    def introMember2(self):
        return htmlTitle("組員 2 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 吳佳潔<br />
組員學號: 40123102<br />
組員相片:<br /><img src="/static/images/member2.jpg"></img><br />


</body>
</html>
'''

    @cherrypy.expose
    def introMember3(self):
        return htmlTitle("組員 3 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 陳于加<br />
組員學號: 40123107<br />
組員相片:<br /><img src="/static/images/member3.jpg"></img><br />


</body>
</html>
'''
    @cherrypy.expose
    def introMember4(self):
        return htmlTitle("組員 4 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 陳怡樺<br />
組員學號: 40123108<br />
組員相片:<br /><img src="/static/images/member4.jpg"></img><br />


</body>
</html>
'''
    @cherrypy.expose
    def introMember5(self):
        return htmlTitle("組員 5 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 康庭瑋<br />
組員學號: 40123130<br />
組員相片:<br /><img src="/static/images/member5.jpg"></img><br />


</body>
</html>
'''
    @cherrypy.expose
    def introMember6(self):
        return htmlTitle("組員 6 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 謝正利<br />
組員學號: 40123154<br />
組員相片:<br /><img src="/static/images/member6.jpg"></img><br />


</body>
</html>
'''
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {
        '/Brython1.2-20131109-201900':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': _curdir+"/Brython1.2-20131109-201900"
        },
        '/static':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': _curdir+"/static"
        },
        '/downloads':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': _curdir + "/downloads",
        },
    }

# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(Brython(), config = application_conf)
else:
    # 近端執行啟動
    cherrypy.quickstart(Brython(), config = application_conf)